import React from 'react';
import * as reducers from '../../src/reducers';
import { Provider } from 'react-redux';
import passport from 'passport';
import { createStore, combineReducers, applyMiddleware } from 'redux';


import { match, RouterContext } from 'react-router';
import { controllers, passport as passportConfig } from '../db'

const userReservationController = controllers && controllers.reservations;
const usersController = controllers && controllers.users;
const manageController = controllers && controllers.manage;


export default (app) => {

    /*app.get('/admin', ensureAuthenticated, function(req, res){
        console.log('Get Admin')
       return  res.redirect('/admin')
    });*/

    if(usersController) {
        app.post('/login', usersController.login);
        app.post('/signUp', usersController.signUp);
        app.post('/logout', usersController.logout);
    } else {
        console.warn('Failed to load userController');
    }

    if(userReservationController) {
        app.post('/reservation', userReservationController.getReservations);
        app.post('/addReservation', userReservationController.add)
    } else {
        console.warn('Failed to load Reservation controller');
    }

    if(manageController) {
        app.get('/getReservationInfo/:date/:court/:hour', manageController.getInfo);
        app.delete('/cancelReservation/:court/:date/:hour', manageController.cancel);
        app.post('/confirmReservation', manageController.confirm);;
        app.post('/duplicate-reservation', manageController.duplicate)
        app.get('/get-notifications', manageController.getNotifications);
    } else {
        console.warn('Failed to load Manage controller')
    }


}


function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    res.redirect('/login')
}


