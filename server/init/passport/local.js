/*
 Configuring local strategy to authenticate strategies
 Code modified from : https://github.com/madhums/node-express-mongoose-demo/blob/master/config/passport/local.js
 */
import { Strategy as RememberMeStrategy } from 'passport-remember-me';
import { Strategy as LocalStrategy } from 'passport-local';
import { issueToken, consumeRememberMeToken} from '../../db/mongo/passport/rememberMe'
import { passport as dbPassport, controllers } from '../../db';


export default (passport) => {
    if (!dbPassport || !dbPassport.local || !typeof dbPassport.local === 'function') {
        console.warn('passport-local');
        return;
    }

    /*
     By default, LocalStrategy expects to find credentials in parameters named username and password.
     If your site prefers to name these fields differently,
     options are available to change the defaults.
     */
    console.log('Initialize LocalStrategy')
    passport.use(new LocalStrategy({
        usernameField: 'phone'
    }, dbPassport.local));

    passport.use(new RememberMeStrategy(
        function(token, done) {
            consumeRememberMeToken(token, function(err, uid) {
                if (err) { return done(err); }
                if (!uid) { return done(null, false); }

                findById(uid, function(err, user) {
                    if (err) { return done(err); }
                    if (!user) { return done(null, false); }
                    return done(null, user);
                });
            });
        },
        issueToken
    ));

};

