import { Strategy as RememberMeStrategy } from 'passport-remember-me';
import { passport as dbPassport, controllers } from '../../db';
import { issueToken, consumeRememberMeToken} from '../../db/mongo/passport/rememberMe'

export default (passport) => {

    if (!dbPassport || !dbPassport.rememberMe || !typeof dbPassport.rememberMe === 'function') {
        console.warn('Error while initializing remember me');
        return;
    }

    console.log('Initialize RememberMeStrategy')

    passport.use(new RememberMeStrategy(
        function(token, done) {
            consumeRememberMeToken(token, function(err, uid) {
                if (err) { return done(err); }
                if (!uid) { return done(null, false); }

                findById(uid, function(err, user) {
                    if (err) { return done(err); }
                    if (!user) { return done(null, false); }
                    return done(null, user);
                });
            });
        },
        issueToken
    ));
}

