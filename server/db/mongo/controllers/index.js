import reservations from './reservations';
import users from './user';
import manage from './manageReservation'

export { reservations, users };

export default  {
    reservations,
    users,
    manage
}