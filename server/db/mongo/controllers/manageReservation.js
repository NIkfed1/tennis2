import Reservation from '../models/reservations';
import ReservationBasic from './reservations';

export function getNotifications(req, res) {
    if(req.isAuthenticated()) {
         Reservation.find({
             "reservation.status": 'holden'
         })
             .select('date')
             .exec((err, result) => {
             if(err) {
                 console.log('==Error while searching for holden reservations==', err);
                 return res.status(410).json({
                     message: 'Ошибка поиска новых бронирований'
                 })
             } else {
                 console.log('==Holden Reservations successfully founded==', result);
                 return res.json({
                     data: result,
                     message: 'Новые бронирования получены!'
                 })
             }
             })
    } else res.status(401).json({message: 'Не авторизован'})
}

export function confirm(req, res) {
    if(req.isAuthenticated()) {
        console.log('==Confirmation data== ', req.body);

        let defReservation = ReservationBasic.getReservationHeader(req.body.date, req.body.court, req.body.hour),
            reservation = {
                username: req.body.username,
                phone: req.body.phone,
                summ: req.body.price,
                status: req.body.duplicatedDate.length > 1 ? 'hiden' : 'confirmed',
                extra_balls: Boolean(req.body.balls),
                extra_rocket: Boolean(req.body.rocket),
                extra_coach: Boolean(req.body.coach),
                discount: req.body.discount,
            }

        ReservationBasic.findReservation(defReservation).then((searchResult) => {
            if(searchResult == false) {
                console.log('==Reservation to confirm is not found or problems appear while searching==', result, err)
                return res.status(409).json({
                    message: 'Бронь не может быть подтверждена по техническим причинам',
                    error: err
                })
            } else {
                if(searchResult == null) {
                    defReservation.reservation = reservation;
                    defReservation.reservation.duplicatedDate = [];
                    defReservation.reservation.duplicatedDate.push(defReservation.date)

                    ReservationBasic.createReservation(defReservation).then((result) => {
                        console.log('Creating....state',res) //== true
                        return result ? res.json({message : 'Новая бронь успешно создана'})
                            : res.status(409).json({message: 'Ошибка создания новой брони'});
                    })

                } else {
                    console.log('Reservation to confirm has found ', searchResult)
                    searchResult.reservation = reservation;

                    searchResult.save((err) => {
                        if (err) {
                            console.log('==Reservation could not be saved==', err)
                            return res.json({
                                message: 'Бронь не подтверждена',
                                error: err,
                            })
                        } else {
                            console.log('==Reservation successfully confirmed==')
                            return res.json({
                                message: 'Бронирование успешно подтверждено',
                                error: err
                            })
                        }
                    })
                }
            }

        });
    } else res.status(401).json({message: 'Не авторизован'})
}

export function getInfo(req, res) {
    if(req.isAuthenticated()) {
        console.log('==getInfo request is authenticated==', req.params);
        let defReservation = ReservationBasic.getReservationHeader(req.params.date, req.params.court, req.params.hour);

        ReservationBasic.findReservation(defReservation).then((result) => {
            if (result == false) {
                console.log('==GETINFO ERROR. WHILE SEARCHING FOR RESERVATION==')
                return res.status(410).json({
                    message: 'Ошибка при поиске бронирования',
                })
            } else {
                if(result == null) {
                    console.log('==GETINFO ERROR. Reservation not founded==')
                    return res.status(409).json({
                        message: 'Бронь не найдена',

                    })
                } else {
                    console.log('==Well done==')
                    return res.json({
                        message: 'Найдена бронь!',
                        result
                    })
                }
            }
        })

    } else {
        console.log('==getInfo request isn\'t authenticated properly==');
        return res.status(401).json({message: 'Запрос брони невозможен! Авторизуйтесь!'})
    }
}

export function cancel(req, res) {
    console.log('==Cancel Reservation request data==', req.params)
    if (req.isAuthenticated()) {
        let defReservation = ReservationBasic.getReservationHeader(req.params.date, req.params.court, req.params.hour)
        console.log('==Prepared cancel data==', defReservation)
        ReservationBasic.findReservation(defReservation).then((result) => {

            if (result == false || result == null) {
                return res.status(409).json({
                    message: 'Нельзя удалить несуществующую бронь'
                })

            } else {
                result.remove((err) => {} )
                return res.json({
                    message: 'Бронь успешно удалена'
                })
            }
        })
    }
}

export function duplicate(req, res) {
    if(req.isAuthenticated()) {
        if(req.body.duplicate.length <= 1) {
            console.log('There is no new duplications');
            return res.status(409).json({message:'Новые даты для дублирования не выбраны'})
        }

        let rootReservation = ReservationBasic.getReservationHeader(req.body.date, req.body.court, req.body.hour);
            ReservationBasic.findAndDuplicateReservations(req.body.duplicate, rootReservation.court, rootReservation.hour, rootReservation.date).then((result) => {
                    console.log('==Slots for duplicate is free. Go ahead==');
                    return res.json({message: 'Бронь успешно дублирована!'})
            })
                .catch(function () {
                    console.log('==Error. Possible dates are busy=');
                    return res.status(409).json({message: 'Выбранные даты заняты!'})
                })
    } else {
        console.log('==getInfo request isn\'t authenticated properly==');
        return res.status(401).json({message: 'Запрос брони невозможен! Авторизуйтесь!'})
    }
}

/*duplicate: this.state.duplicatedMonth,
    date: this.state.date,
    hour: this.state.hour,
    court: this.state.court*/

export default {
    duplicate,
    confirm,
    getInfo,
    cancel,
    getNotifications
}