import Reservation from '../models/reservations';
import { Courts } from '../../../../src/components/Storage';

var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'Mail.Ru',
    auth: {
        user: 'koronakld@mail.ru',
        pass: 'qazwsxedc8',
    }
});

var mailOptions = {
    from: 'koronakld@mail.ru',
    to: 'koronakld@mail.ru',
    subject: 'У вас новая бронь!',
    text: 'У вас новая бронь!'
};

function courtForming(reservations, d) {
    console.log('Get reservations result', d)
    let courtOrigin = Courts(),
        courtDefault = JSON.stringify(courtOrigin);
        courtDefault = JSON.parse(courtDefault);
        courtOrigin = null;
    //let reserve = reservations;
    let date = new Date(d);
    if (reservations.length == 0 || reservations == undefined) {
        var courtCounter = 0;

        courtDefault.forEach(function (court, counter) {

            courtDefault[counter].day = date.getDate(),
                courtDefault[counter].month = date.getMonth(),
                courtDefault[counter].date = date,
                courtDefault[counter].year = date.getFullYear(),
                courtDefault[counter]._id = 0;
            console.log('Empty object. Default is defined')
            //return court;
            courtCounter++;
        })
        console.log('Default object')
        return courtDefault;
    }

    if(reservations)

    reservations.forEach(function (court) {

        if (court == undefined || court.reservation == undefined) {
            console.log('Default court')
            let date = new Date();
            courtDefault[court.court].day = date.getDate(),
                courtDefault[court.court].month = date.getMonth(),
                courtDefault[court.court].year = date.getFullYear(),
                courtDefault[court.court].date = court.date;
                courtDefault[court.court]._id = 0;
            console.log('Empty object. Default is defined')
            //return court;
        }


        courtDefault[court.court].court = court.court;
        courtDefault[court.court].day = court.day;
        courtDefault[court.court].date = court.date;
        courtDefault[court.court].month = court.month;
        courtDefault[court.court].year = court.year;
        courtDefault[court.court]._id = court._id;
        console.log('court state π1')

        courtDefault[court.court].data.forEach(function (schedule) {
                if (schedule.hour == court.hour) {
                    schedule.status = court.reservation.status;
                    schedule._id_ = court._id;
                    schedule.value = court.reservation.summ;

            }
        });
        console.log('Reservation schedule for ' + court.day + '.' + court.month)

    })

        return courtDefault;

}

export function getReservations(req, res) {
    console.log('====getReservations req.data====', req.body)
    let date = req.body.date != undefined ? new Date(req.body.date) : new Date(),
        day = date.getDate(),
        month = date.getMonth(),
        year = date.getFullYear();


    Reservation.find({
        day:day,
        month:month,
        year:year,
    }).exec((err, reservations) => {
        if (err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong');
        }

        res.header('Access-Control-Allow-Methods', 'POST,GET,OPTION')
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Origin-Headers', 'Origin, Content-Type, Accept');

        console.log('====Success in query___getReservations====');
        console.log('==Get Reservation date==', date);
        res.json({date:date, data:courtForming(reservations, req.body.date)});
    })
}

export function  getReservationHeader(date, court, hour) {
    let dt = new Date(date);
        dt.setHours(hour);
    let index = new Date(dt.getFullYear(), dt.getMonth(),dt.getDate(), hour)

    console.log('Date object is ', index)
    return {
        "day": dt.getDate(),
        "month": dt.getMonth(),
        "year":  dt.getFullYear(),
        "court": court,
        "hour": hour,
        "date": index,
        //"index": Number(index) + court
    }
}


export function createReservation(reservation) {

    return new Promise(function(resolve, reject) {
        Reservation.create(
            reservation
            , (err, result) => {
                if (err) {
                    console.log('Ошибка создания брони!',err);
                    reject(false);
                } else {
                    console.log('==Reservation successfully created & confirmed==', result)
                    resolve(true);
                }
            })
    })
}

export function findReservation(searchDateDetails) {
    //let searchDateDetails = getDateObject(Number(req.body.date), Number(req.body.court))
    console.log('==SearchDate Details==', searchDateDetails)
    return new Promise(function(resolve, reject) {
        Reservation.findOne({
            "day": searchDateDetails.day,
            "month": searchDateDetails.month,
            "year": searchDateDetails.year,
            "court": searchDateDetails.court,
            "hour": searchDateDetails.hour
        }).exec((err, result) => {
            if (err) {
                console.log('==Error while searching or empty result==', err)
                return reject(false);
            } else {
                    console.log('==Reservation founded==', result)
                    return resolve(result);
            }

        })
    })
}

export function findAndDuplicateReservations(reservations, court, hour, rootDate) {
    console.log('====Search dates is ', reservations, 'Root date is', rootDate);
    let newReservations = [];

    return new Promise(function(resolve, reject) {
        Reservation.find({
            "court": court,
            "hour": hour
        })
            .where('date').in(reservations)
            //.where('date').ne(rootDate)
            .exec((err, result) => {
            if(err) {
                console.log('==Error while searching reservations==')
                return reject(false)
            } else {
                console.log('==Slots are free. Continue to save duplicates', result, result.length, result.date, rootDate)
                if(result.length == 1 && Number(result[0].date) == Number(rootDate)) {
                    result[0].reservation.duplicatedDate = reservations;
                    result[0].reservation.status = 'hiden';
                    //delete  result[0]["_id"];
                    //newReservations.push(result[0]);
                    //console.log('Root result without _id', result)

                    reservations.forEach((duplicate) => {
                        //console.log('==Duplicate loop', duplicate, result.date, Number(new Date(duplicate)), Number(result.date))
                        if(Number(new Date(duplicate)) == Number(rootDate)) {
                            console.log('==Skiping root reservation while saving duplicates==')
                        } else {
                            let temp = getReservationHeader(duplicate,court,hour);
                                temp.reservation = result[0].reservation;
                                newReservations.push(temp);
                            console.log('==New Reservation==', newReservations)
                        }
                    })
                    Reservation.insertMany(newReservations, (err) => {
                        console.log('Saved with error', err)
                        if(err) {
                            return reject(false);
                        } else {
                            result[0].save(result, (err, updated) => {
                                console.log('Root saved with error ', err, updated)
                                if(err) {
                                    return reject(false);
                                } else {
                                    return resolve(true)
                                }
                            })
                        }
                    })
                } else {
                    console.log('==Searching result for free dates is bad==', result)
                    reject(false)
                }
            }
            })
    })
}

export function add(req, res) {

    console.log('Add reservation req', req.body)
    let reservationHeader = getReservationHeader(req.body.date, req.body.court, req.body.hour),
        reservation = {
            day: reservationHeader.day,
            month: reservationHeader.month,
            year: reservationHeader.year,
            court: reservationHeader.court,
            hour:reservationHeader.hour,
            date: reservationHeader.date,
            index: reservationHeader.index,
            reservation : {
                username: req.body.userInfo,
                phone: req.body.userPhone,
                summ: req.body.summ,
                status: 'holden',
                duplicatedDate: []
            }
        };

        reservation.reservation.duplicatedDate.push(reservationHeader.date);

        console.log('reservation Header', reservationHeader)

    findReservation(reservationHeader).then((result) => {
        if(result == false ) {
            res.status(409).json({ message: "Бронь не найдена"})
        } else {
            if(result == null) {
                createReservation(reservation).then((result) => {
                    if(result) {

                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                console.log('Email error', error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });

                        res.json({message: 'Время забронировано!'})
                    } else {
                        res.status(409).json({message: "Резервирование не удалось"})
                    }
                })
            } else {
                res.status(409).json({message: 'Запрос не авторизован'})
            }
        }
    })
}

export default {
    getReservations,
    add,
    getReservationHeader,
    findReservation,
    findAndDuplicateReservations,
    createReservation
}