import User from '../models/user';
import passport from 'passport';
import { issueToken } from '../passport/rememberMe';


/**
 * POST /login
 */

export function login(req, res, next) {
    // Do email and password validation for the server
    passport.authenticate('local', (authErr, user, info) => {
        if (authErr) return next(authErr);
        if (!user) {
            console.log('Login', user, info, authErr, req.body)
            return res.status(401).json({ message: info.message });
        }
        // Passport exposes a login() function on req (also aliased as
        // logIn()) that can be used to establish a login session

            // Issue a remember me cookie if the option was checked
            issueToken(user, (err, token) => {
            if (err) { return next(err); }
            res.cookie('remember_me', token, { path: '/', httpOnly: true, maxAge: 604800000 });

            return req.logIn(user, (loginErr) => {
                if (loginErr) return res.status(401).json({ message: loginErr });

                console.log(req.isAuthenticated())


                return res.status(200).json({
                    message: 'You have been successfully logged in.',
                    authState: req.isAuthenticated()
                });
            });
        })
    })(req, res, next);
}

/**
 * POST /logout
 */
export function logout(req, res) {
    // Do email and password validation for the server
    req.logout();
    res.redirect('/').json({_id:0,
        phone: 'Гость',
        authState: req.isAuthenticated()})
}

/**
 * POST /signup
 * Create a new local account
 */
export function signUp(req, res, next) {
    console.log('signUp req', req.body)
    let user = new User({
        phone: req.body.phone,
        password: req.body.password
    });

    console.log('User object', user)
    User.findOne({ phone: req.body.phone }, (findErr, existingUser) => {
        console.log('excisting user', existingUser)
        if (existingUser) {
            return res.status(409).json({ message: 'Account with this phone already exists!' });
        }

        return user.save((saveErr) => {
            if (saveErr) return next(saveErr);
            return req.logIn(user, (loginErr) => {
                if (loginErr) return res.status(401).json({ message: loginErr });
                return res.status(200).json({
                    user,
                    message: 'Account has been succesfully added',
                    authState: req.isAuthenticated()
                    }
                );
            });
        });
    });
}




export default {
    login,
    signUp,
    logout
}
