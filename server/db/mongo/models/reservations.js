import mongoose from 'mongoose';

const ReservationsSchema = new mongoose.Schema({
    day: Number,
    month: Number,
    year: Number,
    court: Number,
    hour: Number,
    date: Date,
    reservation: {
        username: String,
        phone: Number,
        summ: Number,
        status: String,
        duplicatedMonth: {
            type: Array,
            default: []
        },
        duplicatedDate: {
            type: Array,
            default: []
        },
        discount: {
            type: Number,
            default: 0
        },
        extra_coach: {
            type: Boolean,
            default: false
        },
        extra_rocket: {
            type: Boolean,
            default: false
        },
        extra_balls: {
            type: Boolean,
            default: false
        },
        totalSumm: Number
    }
});



export default mongoose.model('Reservation', ReservationsSchema);