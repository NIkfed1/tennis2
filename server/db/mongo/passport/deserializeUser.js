import User from '../models/user';

export default (id, done) => {
    User.findById(id, (err, user) => {
        err
        ? done(err)
        : done(null, user);
    });
};
