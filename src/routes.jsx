import React from 'react';
import { IndexRoute, Route} from 'react-router';

import App from './components/App';
import LayoutAdmin from './components/admin/LayoutAdmin';
import LayFooter from './components/LayFooter';
import AdminLogin from './components/admin/AdminLogin';

export default (store) => {
    const requireAuth = (nextState, replace, callback) => {
        const {user: {authenticated}} = store.getState();
        console.log('Require Auth front', store.getState())
        if (!authenticated) {
            replace({
                pathname: '/login',
                state: {nextPathname: nextState.location.pathname}
            });
        }
        callback();
    };

    const redirectAuth = (nextState, replace, callback) => {
        const {user: {authenticated}} = store.getState();
        if (authenticated) {

            replace({
                pathname: '/admin'
            });
        }
        console.log('Redirect Auth', store.getState())
        callback();
    };
    return (
        <Route path="/">
            <IndexRoute component={App}/>
            <Route path="login" component={AdminLogin} onEnter={redirectAuth}/>
            <Route path="admin" components={LayoutAdmin} onEnter={requireAuth}/>
        </Route>
    );
};
