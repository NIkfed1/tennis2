export function getWeekDayNames(date) {
    date = new Date(date) || new Date
    let dayNames = ['Вс', 'Пн', 'Вт', 'Ср', 'Чв', 'Пн', 'Сб'],
        days = date.getDay();
    return dayNames[days];
}

export function getMonthNames(date) {
    date = new Date(date) || new Date();
    let monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        month = date.getMonth();
    return monthNames[month];
}

export function getMonthNames2(date) {
    date = new Date(date) || new Date();
    let monthNames = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
        month = date.getMonth();
    return monthNames[month];
}

export function getWeekDayNames2(date) {
    date = new Date(date) || new Date
    let dayNames = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        days = date.getDay();
    return dayNames[days];
}