import * as types from '../types'
import { List } from 'immutable';

const defaultState = false;

export default function todoReducer(state = defaultState, action) {
    switch(action.type) {

        case types.GET_DEFAULT_DATE:
            return action;

        default:
            return state;
    }
}


