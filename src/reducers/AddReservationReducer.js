import * as types from '../types'
import { List } from 'immutable';

const defaultState = false;

export default function AddReservation(state = defaultState, action) {
    switch(action.type) {

        case types.ADD_RESERVATION:
            return action;

        default:
            return state;
    }
}