import { combineReducers } from 'redux';
import * as types from '../types';
import { List } from 'immutable';

const defaultState = false;

const canceledReserve = (state = defaultState, action) => {
    switch(action.type) {
        case types.CANCELED_RESERVE:
            return action;

        default:
            return state;
    }
}

const duplicated = (state = defaultState, action) => {
    switch(action.type) {
        case types.DUPLICATED_MONTHS:
            return action;
        case types.DUPLICATED_DATES:
            return action;
        case types.DUPLICATED_ERROR:
            return action;

        default:
            return state;
    }
}

const confirmed = (state = defaultState, action) => {
    switch(action.type) {

        case types.CONFIRMED_RESERVATION:
            return action;

        default:
            return state;
    }
}

const reservationInfo = (state = defaultState, action) => {
    switch(action.type) {

        case types.RESERVATION_INFO:
            return action;

        default:
            return state;
    }
}

const notifications = (state = defaultState, action) => {
    switch(action.type) {

        case types.NOTIFICATIONS_RECEIVED:
            return action;

        default:
            return state;
    }
}

const manage = combineReducers({
    confirmed,
    reservationInfo,
    duplicated,
    canceledReserve,
    notifications
});

export default manage;