import { push } from 'react-router-redux';
import { universalService } from '../service';

import * as types from '../types';

function Reserve(res) {
    return {
        type: types.ADD_RESERVATION,
        status: res.status,
        message: res.data.message
    }
}

export function addReserve(date, court, username, userPhone, hour, summ, status) {
    console.log('COurt number', court)
    let reservation = {
        date: date,
        court:court,
        userInfo: username,
        userPhone: userPhone,
        summ: summ,
        hour: hour,
        status: status
    };
    return (dispatch, getState) => {
        return universalService().addReserve({
            date: date,
            court:court,
            userInfo: username,
            userPhone: userPhone,
            summ: summ,
            hour: hour,
            status: status
        }).then((res) => {
            console.log('response of addReserve', res)
            if (res.status == 200) {
                dispatch(Reserve(res))
            }
        })
            .catch(() => {
                console.log('==Add Error==');
                dispatch(Reserve({
                    status: 409,
                    data: {
                        message: 'Ошибка!'
                    }
                }))
            });
    }
}