import { push } from 'react-router-redux';
import { universalService } from '../service';

import * as types from '../types';

function Date(date, data) {
    return {
        type: types.GET_DEFAULT_DATE,
        date: date,
        data: data,
    }
}

export function getDefaultDate(date) {
    return (dispatch, getState) => {

        return universalService().getDefaultDate({date}).then((res) => {
            console.log('response of getDefaultDate', res.status == 200)
            if (res.status == 200) {
                dispatch(Date(date,res.data))
            }
        })
        .catch(() => {
            console.log({ date, error: 'Oops! Something went wrong and we couldn\'t get daf date'});
        });
    }
}


