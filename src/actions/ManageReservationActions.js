import { push } from 'react-router-redux';
import { universalService } from '../service';
import { getDefaultDate } from './ToDoActions';

import * as types from '../types';

function confirmedReserve(data) {
    return {
        type: types.CONFIRMED_RESERVATION,
        data: data.data.message,
        status: data.status
    }
}

function reservationInfo(data) {
    return {
        type: types.RESERVATION_INFO,
        data: data
    }
}

function duplicated(data) {
    return {
        type: types.DUPLICATED_DATES,
        message: data.data.message,
        status: data.status
    }
}

function notifications(data) {
    return {
        type: types.NOTIFICATIONS_RECEIVED,
        data: data

    }
}


function canceledReserve(data) {
    return {
        type: types.CANCELED_RESERVE,
        message: data.data.message,
        status: data.status
    }
}


export function duplicate(data) {
    return (dispatch, getState) => {

        console.log('Duplicate dates', data)
        return universalService().duplicate(data).then((res) => {
            console.log('Response of duplicate', res.status == 200)
            if (res.status == 200) {
                return dispatch(duplicated(res))
            }
        })
            .catch(() => {
                console.log('==Duplicate Error==');
                return dispatch(duplicated({
                    status: 409,
                    data: {
                        message: 'Ошибка!'
                    }
                }))
            });
    }
}

export function cancelReserve(data) {
    return (dispatch, getState) => {

        console.log('Cancel date', data)
        return universalService().cancelReserve(data.date, data.hour, data.court).then((res) => {
            console.log('response of cancelReserve', res)
            if (res.status == 200) {
                return dispatch(canceledReserve(res))
            }
        })
            .catch(() => {
                console.log({
                    date,
                    message: 'Oops! Something went wrong and we couldn\'t cancel Reservaton'});
            });
    }
}

export function getReservationInfo(hour, court, date) {
    return (dispatch, getState) => {

        return universalService().getReservationInfo(hour, court, date).then((res) => {
            console.log('response of getReservationInfo', res.status == 200)
            if (res.status == 200) {
               return dispatch(reservationInfo(res.data))
            }
        })
            .catch(() => {
                console.log({
                    date,
                    message: 'Oops! Something went wrong and we couldn\'t get Reservaton Info'});
            });
    }
}


export function confirmReserve(data) {

    return (dispatch, getState) => {
        return universalService().confirmReserve(data).then((res) => {
            console.log('response of addReserve', res.data)
            if (res.status == 200) {
                return dispatch(confirmedReserve(res))
            }
        })
            .catch(() => {
                console.log({ error: 'Oops! Something went wrong and we couldn\'t confirm reservation'});
            });
    }
}

export function getNotifications() {

    return (dispatch, getState) => {

        return universalService().getNotifications().then((res) => {
            console.log('response of getNotifications', res.status == 200)
            if (res.status == 200) {
                return dispatch(notifications(res.data))
            }
        })
            .catch(() => {
                console.log({
                    message: 'Oops! Something went wrong and we couldn\'t get Notifications Info'});
            });
    }

}