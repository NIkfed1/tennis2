import { apiEndpoint } from '../../config/app';
import createRestApiClient from '../utils/createRestAPIClient';

export default () => {
    const client = createRestApiClient().withConfig({ baseURL: apiEndpoint });
    return {
        getDEfaultDate: () => client.request({
            method: 'GET',
            url: '/topic'
        }),
        deleteTopic: ({ id }) => client.request({
            method: 'DELETE',
            url: `/topic/${id}`
        }),
        updateTopic: ({ id, data }) => client.request({
            method: 'PUT',
            url: `/topic/${id}`,
            data
        }),
        createTopic: ({ id, data }) => client.request({
            method: 'POST',
            url: `/topic/${id}`,
            data
        }),
        getDefaultDate: ({date, admin}) => client.request({
            method: 'POST',
            url: '/reservation',
            data: {date, admin}
        }),
        addReserve: (reservation) => client.request({
            method: 'POST',
            url: '/addReservation',
            data: reservation
        }),
        confirmReserve: (data) => client.request({
            method: 'POST',
            url: '/confirmReservation',
            data: data
        }),
        getReservationInfo: (hour, court, date) => client.request({
            method: 'GET',
            url: `/getReservationInfo/${date}/${court}/${hour}`,
            //data: {hour, court, date}
        }),
        cancelReserve: (date, hour, court) => client.request({
            method: 'DELETE',
            url: `/cancelReservation/${court}/${date}/${hour}`,
            //data: data
        }),
        duplicate: (data) => client.request({
            method: 'POST',
            url: '/duplicate-reservation',
            data: data
        }),
        getNotifications: () => client.request({
            method: 'GET',
            url: '/get-notifications'
        })
    };
};