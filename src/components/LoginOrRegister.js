import React, { Component } from 'react';
import autobind from 'react-autobind';
import Formsy from 'formsy-react';
import { observer } from 'mobx-react';
import MyOwnInput from './forms/inputs';

import './css/LoginOrRegister.css';
import './css/App.css';

export default class LoginOrRegister extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            errorMessage: '',
            regTitle: 'Регистрация',
            logTitle: 'Авторизация',
            isAuthorizes: false,
            canSubmit : true,
            registerFormLink: true,
            loginFormLink: false,
        };
        autobind(this);
    }

    componentWillUpdate(nextProps) {
        console.log('LoginOrRegister ComponentWillUpdate', nextProps)
        if(nextProps.user.authenticated) {
            this.props.loginPage(false);
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log('LoginOrRegister ComponentWillReceiveProps', nextProps)
            this.setState({
                errorMessage: nextProps.user.message
            })
    }

    cancelLoginPage = (e) => {
        this.props.loginPage(false);
    }

    enableAuthButton() {
        this.setState({
            canSubmit: true
        });
    }

    loginForm() {

        console.log('LoginOrRegister loginForm')
        this.setState({
            loginFormLink: true,
            registerFormLink: false
        })
    }

    registerForm() {

        console.log('LoginOrRegister registerForm')
        this.setState({
            loginFormLink: false,
            registerFormLink: true
        })
    }

    disableAuthButton() {
        this.setState({
            canSubmit: false
        });
    }

    submitAuth(model) {
        console.log('auth data', model, this.state.registerFormLink)
        if (this.state.registerFormLink) {
            this.props.signUp(model);
        } else {
            this.props.manualLogin(model, false);
        }
    }

    render() {
        return (

            <div className="App">
              <div className="registerOrLogin">
                  <div className="form_header">
                  <a className={this.state.registerFormLink ? 'active':'inactive'} onClick={this.registerForm}>{this.state.regTitle}</a >
                  <a className={this.state.loginFormLink ? 'active':'inactive'} onClick={this.loginForm}>{this.state.logTitle}</a>
                  </div>

                  <div className="error">{this.state.errorMessage}</div>
              </div>
            </div>
        )

    }
}

