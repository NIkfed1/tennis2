import React, { Component, Proptypes } from 'react';
import Modal, {closeStyle} from 'simple-react-modal';
import autobind from 'react-autobind';
import { observer } from 'mobx-react';
import { DateField, Calendar } from 'react-date-picker';
import { getWeekDayNames, getMonthNames} from '../utils/dateHelper'

import 'react-date-picker/index.css'
import './css/LayHeaderFlex.css';

export default class LayHeader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isAdmin: false,
            authenticated: false,
            userName: 'Гость',
            adminUserName: 'Администратор',
            userId: 0,
            userPhone: '',
            currentDate: null,
            day: null,
            weekDay: null,
            monthNames: null,
            open: false,
            canSubmit: true,
            notifications: false,
            notifyCount: 0
        }
        this.props.getDefaultDate(Number(new Date()));
        autobind(this);

        //console.log('Props log', this.props)

    }

    componentWillUpdate(nextProps) {
        if(this.props != nextProps) {
            this.setState({
                currentDate: nextProps.todos.date,
                day : new Date(nextProps.todos.date).getDate(),
                weekDay: getWeekDayNames(nextProps.todos.date),
                monthNames: getMonthNames(nextProps.todos.date),

            });

            console.log('LayHeader ComponentWillUpdate', nextProps)
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log('LayHeader ComponentWillReceiveProps Received', nextProps);
        if (nextProps.user) {
            console.log('LayHeader ComponentWillReceiveProps Received', nextProps.user);
            this.setState({
                isAdmin: nextProps.admin,
                authenticated: nextProps.user.authenticated
            })
        }
    }

    show() {
       this.setState({
           open: true
       })
    }

    close() {
        this.setState({
            open: false,
        });
    }

    handleNext = (e) => {
        console.log('handleNext prev. date',new Date(this.state.currentDate)  )
        let d = new Date(this.state.currentDate)
            .setDate(new Date(this.state.currentDate).getDate() + 1);
        console.log('handleNext next date',new Date(d))
        this.props.getDefaultDate(d);
    }

    handlePrev = (e) => {
        console.log('handlePrev',this.state.currentDate )
        let d = new Date(this.state.currentDate)
            .setDate(new Date(this.state.currentDate).getDate() - 1);
        this.props.getDefaultDate(d);
    }

    loginPage = (e) => {
        this.props.loginPage(true);
    }

    registerPage = (e) => {
        this.props.loginPage(true);
    }

    onChange = (dateString, {dateMoment, timestamp}) => {

        console.log('OnChange', new Date(dateString))
        this.props.getDefaultDate(Number(new Date(dateString)));
        this.close();
    }


    logout() {
        console.log('LayHeader logout', this.props)
        this.props.logOut(this.state.isAdmin);

    }

    render() {

        return (
        <div>
            <div className="LayHeader">
                <div className="LayHeader__main LayHeader_container">
                    <div className="LHcontainer">
                        <div className="LHitem item__l">
                           <div>
                             <span className="hadr">Александра Суворова, 56</span><br />
                             <span className="hras">Ежедневно 06:00-24:00, Тел.+7(4012) 50-78-76</span>
                           </div>
                         </div>
                        <div className="LHcontainer__sub item__d LayHeader__day">
                              <div className="LHitem item__la" onClick={this.handlePrev}></div>
                              <div className="LHitem item__de" onClick={this.show}>
                                <div className="LHcenter__day" >{this.state.day}</div>
                                <div className="LHcenter__weekday" >{this.state.weekDay}</div>
                              </div>
                              <div className="LHitem item__ra" onClick={this.handleNext}></div>
                            </div>

                        <div className="LHitem item__c" onClick={this.show}></div>
                    </div>
                </div>
                <div className="LayHeader__date LayHeader_container">
                    <div className="LayHeader__arrow">
                        <span className="LayHeader__currentdate-week" onClick={this.show}>{this.state.day} {this.state.weekDay}. {this.state.monthNames}</span>
                        <span className="LayHeader__currentdate" onClick={this.show}>{this.state.monthNames}</span>
                    </div>
                </div>
                <div>
                   <Modal containerStyle={{ background: 'white', width: '325px',height: '300px', padding: 2, borderRadius:'6px'}} closeOnOuterClick={true} show={this.state.open} onClose={this.close}>
                     <Calendar dateFormat="YYYY-MM-DD" date={this.state.currentDate} onChange={this.onChange} className="Calendar"/>
                   </Modal>
                </div>
            </div>
         </div>
        );
    }
}


