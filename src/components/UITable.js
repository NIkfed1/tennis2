import _ from 'lodash';
import React, { Component } from 'react';
import { observer } from 'mobx-react';
import autobind from 'react-autobind';
import Loader from 'react-loader';

import Formsy from 'formsy-react';
import MyOwnInput from './forms/inputs';
import Modal, {closeStyle} from 'simple-react-modal';
import './css/UITable.css';

class UIRow extends Component {
    constructor(props) {
        super(props)
    }


    statuses = {
        holden: 'Арендовано',
        free: 'Свободно',
        hiden: 'Длительная аренда',
        confirmed: 'Арендовано'
    }

    icons = {
        holden: 'UIpic__leased',
        free: 'UIpic__free',
        hiden: 'UIpic__playball',
        confirmed: 'UIpic__leased'
    }

    render() {
        return (
            <tr className={this.props.data.status} onClick={this.props.showed.bind(null, this.props.data)}> {/*onTouchStart={this.props.showed.bind(null, this.props.data)}*/}
                <th>{this.props.data.timeslot}</th>
                <td>{this.props.data.status != 'free' ? '' : this.props.data.value}  {this.statuses[this.props.data.status]}</td>
                <td className="UIclear"><div className={this.icons[this.props.data.status]} /></td>
            </tr>
        );
    }
}


class UITable extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            open: false,
            openErr: false,
            canSubmit: true,
            slot: '',
            hour: '',
            value: '',
            court: props.court,
            date: null,
            schedule: null,
            currentUIRowStatus: null,
            loaded: false,
            errorMessage: ''
        };
        autobind(this);
        console.log('UITable constructor')
    }

    componentWillUpdate(nextProps) {
        if(this.props !== nextProps) {
            console.log('UITable componentWillUpdate', nextProps)
            this.setState({
                date: nextProps.todos.data.date,
                court: nextProps.court

            })
        }

        if(this.props.addReservation !== nextProps.addReservation) {

            console.log('UITABLE ComponentWillReceiveProps Received. Add Reserve', nextProps.addReservation);
            this.setState({
                loaded: true,
                errorMessage: nextProps.addReservation.message,
            })
            if(nextProps.addReservation.status == 200) {
                //this.state.currentUIRowStatus.status = 'holden';
                //this.setState({currentUIRowStatus: { status: 'holden'}})
                this.close()
            }

        }

    }


    componentWillReceiveProps(nextProps) {
        if(this.props.todos !== nextProps.todos) {

                console.log('UITABLE ComponentWillReceiveProps Received Get Date', nextProps.todos);
                this.setState({
                    date: nextProps.todos.data.date,
                    schedule: nextProps.todos.data.data[nextProps.court].data,
                    court: nextProps.todos.data.data[nextProps.court].court,
                    loaded: true
                })
            console.log('Court number',nextProps.court);
        }
    }

    close() {
        console.log('close')
        this.setState({
            loaded: true,
            open:false,
            errorMessage: ''
        });
        this.props.getDefaultDate(this.state.date)
    }

    closeErr() {
        console.log('close err');
        this.setState({
            openErr: false
        })
    }

    showed(slot) {
        console.log('Court number', this.state.court)
        if(slot.status == 'holden' || slot.status == 'confirmed' || slot.status == 'hiden') {
            this.setState({
                openErr: open
            })
        } else {
            console.log('show slot', slot);
            this.setState({
                open: true,
                slot: slot.timeslot,
                hour: slot.hour,
                value: slot.value,
                court: this.state.court,
                currentUIRowStatus: slot
            });
        }
    }
    enableButton() {
        this.setState({
            canSubmit: true
        });
    }
    disableButton() {
        this.setState({
            canSubmit: false
        });
    }
    submit(model) {

        console.log('Add Date', this.state.date)
        this.setState({
            //currentUIRowStatus: this.state.currentUIRowStatus.status = 'holden'
            loaded: false
        })

        this.props.addReserve(
            this.state.date,
            this.state.court,
            model.username,
            model.phone,
            this.state.hour,
            this.state.value,
            this.state.currentUIRowStatus.status
        )
        //this.props.getDefaultDate(this.state.date);
        //this.close(true);
    }
    render() {

        const _this = this;

        return (
            <div className="UITable">
                {this.state.loaded ? '' : <Loader loaded={this.state.loaded}/>}
                <Modal className="ReservationPopup" closeOnOuterClick={true} show={this.state.open} onClose={this.close}>
                <div className="popup">
                    <div className="form__header">Чтобы записаться оставьте свои контактные данные
                    </div>
                    <Formsy.Form onValidSubmit={this.submit} onValid={this.enableButton} onInvalid={this.disableButton}>
                        <MyOwnInput autoFocus={false} defStyle="smart__field--tel" placeholder="Напишите телефон для связи с Вами" name="phone" required/><br/>
                        <MyOwnInput autoFocus={false} defStyle="smart__field--guest" placeholder="Введите имя и фамилию" name="username" />
                        <button className="form__button--submit" type="submit" disabled={!this.state.canSubmit}>Ok</button>
                        <div className="error-message">{this.state.errorMessage}</div>
                    </Formsy.Form>
                </div>
                </Modal>

                <Modal className="ReservationPopup" closeOnOuterClick={true} show={this.state.openErr} onClose={this.closeErr}>
                    <div className="popup">
                    <div className="form__header">Извините, но это время уже занято, выберите другое удобное для Вас.</div>
                    <Formsy.Form onValid={this.enableButton}>
                        <button className="form__button--submit" type="submit" onClick={this.closeErr}>Ok</button>
                    </Formsy.Form>
                    </div>
                </Modal>
                <table className="ui compact unstackable table">
                    <thead>
                    <tr>
                        <th><div className="UIpic__time"></div></th>
                        <td><div className="UIpic__cord"><div className="UIpic__num">Корт</div></div></td>
                        <td>{this.state.court+1}</td>
                    </tr>
                    </thead>
                    <tbody>

                    {_.map(this.state.schedule, function (object, i) {
                        return <UIRow showed={_this.showed}
                                          data={object}
                                          key={object._id_}
                                          />;
                    })
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default UITable;
