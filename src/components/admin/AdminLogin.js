import React, { Component } from 'react';
import autobind from 'react-autobind';
import Formsy from 'formsy-react';
import MyOwnInput from '../forms/inputs';
import { observer } from 'mobx-react';

import { connect } from 'react-redux';
import { manualLogin } from '../../actions/AuthActions';

import '../css/App.css';
import '../css/LoginOrRegister.css';
import '../css/UITable.css'



class AdminLogin extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            errorMessage: '',
            logTitle: 'Авторизация',
            canSubmit : true,
        };
        autobind(this);
    }

    componentWillUpdate(nextProps) {
        console.log('AdminLogin ComponentWillUpdate', nextProps)
    }

    componentWillReceiveProps(nextProps) {
        console.log('AdminLogin ComponentWillReceiveProps', nextProps)
        this.setState({
            errorMessage: nextProps.user.message
        })
    }

    cancelLoginPage = (e) => {
        window.location.replace('/')
    }

    disableAuthButton() {
        this.setState({
            canSubmit: false
        });
    }

    submitAuth(model) {
        console.log('auth data', model)
        this.props.manualLogin(model, true);

    }

    render() {
        return (
            <div>
            <div className="App">
                <div className="registerOrLogin">
                    <div className="form_header">
                        <a className='active'>{this.state.logTitle}</a>
                    </div>
                    <div>
                    <Formsy.Form onValidSubmit={this.submitAuth} onValid={this.enableButton} onInvalid={this.disableButton}>
                        <MyOwnInput autoFocus={true} defStyle="smart__field--tel" placeholder="Введите телефон" name="phone" required/><br/>
                        <MyOwnInput autoFocus={false} defStyle="smart__field--password" placeholder="Введите пароль" name="password" required />
                        {/*<input type="checkbox" name="remember_me" value="yes"/><span>Запомнить меня</span><br/>*/}
                        <button className="form__button--submit" type="submit" disabled={!this.state.canSubmit}>Ok</button>
                        <button className="form__button--submit" type="submit" onClick={this.cancelLoginPage}>Отмена</button>
                    </Formsy.Form>
                    </div>
                    <div className="error">{this.state.errorMessage}</div>
                </div>
            </div>
            </div>
        )

    }
}

function mapStateToProps({user}) {
    return {
        user
    };
}

export default connect(mapStateToProps, { manualLogin })(AdminLogin);

