import _ from 'lodash';
import React, { Component } from 'react';
import { observer } from 'mobx-react';
import autobind from 'react-autobind';
import Loader from 'react-loader';

import ManageReservationForm from './ManageReservationForm';
import Modal, {closeStyle} from 'simple-react-modal';
import '../css/UITable.css';

class UIRowAdmin extends Component {
    constructor(props) {
        super(props)
    }


    statuses = {
        holden: 'Бронь',
        free: 'Свободно',
        hiden: 'Длительная аренда',
        confirmed: 'Арендовано'

    }

    icons = {
        holden: 'UIpic__reserved',
        free: 'UIpic__free',
        hiden: 'UIpic__playball',
        confirmed: 'UIpic__leased'
    }

    render() {
        return (
            <tr className={this.props.data.status == 'holden' ? this.props.data.status + '_admin' : this.props.data.status}
                onClick={this.props.showed.bind(null, this.props.data)}>
                <th>{this.props.data.timeslot}</th>
                <td>{this.props.data.status != 'free' ? '' : this.props.data.value}  {this.statuses[this.props.data.status]}</td>
                <td className="UIclear"><div className={this.icons[this.props.data.status]} /></td>
            </tr>
        );
    }
}


class UITableAdmin extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            loaded: false,
            open: false,
            canSubmit: true,
            slot: null,
            court: null,
            date: null,
            schedule: null,
            scheduleSlot: null,
            reservationInfo: false,
            canceledReserve: false,
            confirmed: false,
            currentUIRowStatus: null,
            duplicated: false,
        };
        autobind(this);
        console.log('UITable constructor')
    }

    componentWillUpdate(nextProps) {

            console.log('UITableAdmin componentWillUpdate', nextProps)
            if(this.props !== nextProps) {
                this.setState({
                    date: nextProps.todos.data.date,
                    court: nextProps.court,
                    reservationInfo: nextProps.manage.reservationInfo,
                    canceledReserve: nextProps.manage.canceledReserve,
                    confirmed: nextProps.manage.confirmed,
                    duplicated: nextProps.manage.duplicated,
                })
        }
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.todos != nextProps.todos) {

            console.log('UITABLEAdmin ComponentWillReceiveProps Received', nextProps.todos);
            this.setState({
                loaded: true,
                date: nextProps.todos.data.date,
                schedule: nextProps.todos.data.data[nextProps.court].data,
                court: nextProps.todos.data.data[nextProps.court].court,
            })
            console.log('Court number',nextProps.court);
            console.log('Court object id', nextProps.todos.data.data[nextProps.court]._id)
        }
    }

    close(status) {
        this.setState({
                open: false,
                duplicateResultMessage: false,
                //loaded: false
        });
        this.state.currentUIRowStatus.status = status;
            console.log('close submitted', status);

        //this.props.getDefaultDate(this.state.date);
        this.props.getNotifications()
    }

    closeCancel() {

        this.setState({
            open: false,
            //loaded: false,
            //currentUIRowStatus: this.state.currentUIRowStatus.status = 'free',
        });
    }

    closeErr() {
        console.log('close err');
        this.setState({
            openErr: false
        })
    }

    showed(slot) {
        if(slot.status == 'free') {
            console.log('showed free slop', slot);
            this.setState({
                open: true,
                slot: slot,
                currentUIRowStatus: slot
            });
        } else {
            console.log('showed. Getting Reservation Info', new Date(this.state.currentDate));
            this.props.getReservationInfo(
                slot.hour,
                this.state.court,
                this.state.date
            );
            this.setState({
                open: true,
                slot: slot,
                currentUIRowStatus: slot
            })
        }
    }

    enableButton() {
        this.setState({
            canSubmit: true
        });
    }
    disableButton() {
        this.setState({
            canSubmit: false
        });
    }

    render() {

        const _this = this;

        return (
            <div className="main">
                {this.state.loaded ? '' : <Loader loaded={this.state.loaded}/>}
            <div className="UITable">
                <table className="ui compact unstackable table">
                    <thead>
                    <tr>
                        <th><div className="UIpic__time"></div></th>
                        <td><div className="UIpic__cord"><div className="UIpic__num">Корт</div></div></td>
                        <td>{this.state.court + 1}</td>
                    </tr>
                    </thead>
                    <tbody>

                    {_.map(this.state.schedule, function (object, i) {
                        return <UIRowAdmin showed={_this.showed}
                                      data={object}
                                      key={object._id_}
                        />;
                    })
                    }
                    </tbody>
                </table>
            </div>
                <Modal
                    containerStyle={{padding: 0, borderRadius:'5px', background: '#f4f3ef', width: '602px' }}
                    closeOnOuterClick={true}
                    show={this.state.open}
                    onClose={this.closeCancel}
                >
                    <ManageReservationForm date={this.state.date}
                                           slot={this.state.slot}
                                           court={this.state.court}
                                           reservationInfo={this.state.reservationInfo}
                                           close={this.close}
                                           duplicated={this.state.duplicated}
                                           canceledReserve={this.state.canceledReserve}
                                           confirmed={this.state.confirmed}
                                           getDefaultDate={this.props.getDefaultDate}
                                           updatedSchedule={this.state.schedule}
                                           className="ManageReservationForm"
                    />
                </Modal>
            </div>

        );
    }
}


export default UITableAdmin;

