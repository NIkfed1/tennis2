import React, { Component, PropTypes } from 'react';

import { bindActionCreators } from 'redux'
import * as TodoActions from '../../actions/ToDoActions';
import * as AuthAction from '../../actions/AuthActions';
import * as LoginPageActions from '../../actions/LoginPageActions';
import * as ManageActions from '../../actions/ManageReservationActions';
import { connect } from 'react-redux';

import { observer } from 'mobx-react';
import { IndexRoute, Router, Route, Link, browserHistory, hashHistory } from 'react-router';

import LayHeaderAdmin from './LayHeaderAdmin';
import LayFooter from '../LayFooter';
import UITableAdmin from './UITableAdmin';

import '../css/App.css';
import '../css/index.css';
import '../css/RD.css';


@connect(state => ({
    todos: state.todoss,
    user: state.user,
    loginPage: state.loginPage,
    manage: state.manage
}))
export default class Layout extends Component {
    constructor(props) {
        super(props);
        //console.log('LayoutAdmin. Constructor', props);
    }

    componentWillUpdate(nextProps) {
        //console.log('LayoutAdmin. ComponentWillUpdate', nextProps);
    }

    render() {
        const { todos, user, loginPage, manage, dispatch } = this.props;

        return (
            <div className="App">
                <LayHeaderAdmin todos={todos}
                           user={user}
                           admin={true}
                           manage={manage}
                           {...bindActionCreators(TodoActions, dispatch)}
                           {...bindActionCreators(AuthAction, dispatch)}
                           {...bindActionCreators(ManageActions, dispatch)}/>
                <div className='flexcontainer'>
                    <UITableAdmin todos={todos}
                                 manage={manage}
                                 court={0}
                                  {...bindActionCreators(ManageActions, dispatch)}
                                  {...bindActionCreators(TodoActions, dispatch)}
                    />
                    <UITableAdmin todos={todos}
                                  manage={manage}
                                  court={1}
                                  {...bindActionCreators(ManageActions, dispatch)}
                                  {...bindActionCreators(TodoActions, dispatch)}
                    />

                </div>

                <LayFooter loginPage={loginPage}
                           user={user}
                           admin={false}
                           {...bindActionCreators(AuthAction, dispatch)}
                           {...bindActionCreators(LoginPageActions, dispatch)}/>
            </div>
        );

    }
};
