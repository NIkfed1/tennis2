import React, { Component } from 'react';
import _ from 'lodash';
import { observer } from 'mobx-react';
import autobind from 'react-autobind';

import DayPicker, { DateUtils } from 'react-day-picker';
import { Col, Button, Form, FormGroup, FormControl, ControlLabel, Clearfix, Tab, Row, Nav, NavItem, Table, Column, Checkbox } from 'react-bootstrap';
import Formsy from 'formsy-react';
import MyOwnInput from '../forms/inputs';

import { getMonthNames2, getWeekDayNames2} from '../../utils/dateHelper';

import { connect } from 'react-redux';
import { confirmReserve, cancelReserve, duplicate, getReservationInfo, getNotifications } from '../../actions/ManageReservationActions';
import { getDefaultDate } from '../../actions/ToDoActions';
import Loader from 'react-loader';

import '../css/ManageReservationForm.css';
import "react-day-picker/lib/style.css"
import '../css/UITable.css';
import DuplicateMonths from './DuplicatedMonths';


class ManageReservationForm extends Component {

    constructor(props) {
        super(props);
        console.log('ManageReservationForm constructor', props)
        let selectedDate = new Date(props.date);
        this.state = {
            date: new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate(), props.slot.hour+3),
            slot: props.slot.timeslot,
            hour: props.slot.hour,
            court:props.court,
            status: props.slot.status,
            courtPrice: props.slot.value,
            newPrice: props.slot.value,
            extrasSum: 0,
            coach: false,
            coachPrice: 1000,
            balls: false,
            ballsPrice: 100,
            rocket: false,
            rocketPrice: 300,
            discount: 0,
            months: [0,1,2,3,4,5,6,7,8,9,10,11],
            duplicatedDate: [new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate(), props.slot.hour+3)],
            reservation: null,
            phone: '',
            username: '',
            loaded: false,
            duplicateResultMessage: false,
            header: new Date(props.date).getDate() + ' ' +
            getMonthNames2(props.date) + ' (' +
            getWeekDayNames2(props.date) + ') c ' +
            props.slot.timeslot + '  -  ' + this.props.slot.value + ' р.',
        }
        autobind(this);
    }

    componentDidMount() {
    if(this.state.status == 'free') {
        this.onSuccess();}
    }

    onSuccess() {
        this.setState({loaded: true})
    }

    onError() {
        this.setState({loader: false})
    }

    componentWillUpdate(nextProps) {
        console.log('ManageReservationForm componentWillUpdate', nextProps, this.props)
        if (nextProps.reservationInfo != this.props.reservationInfo) {
         this.setState({
                loaded: true,
                discount: nextProps.reservationInfo.data.result.reservation.discount,
                coach: nextProps.reservationInfo.data.result.reservation.extra_coach,
                balls: nextProps.reservationInfo.data.result.reservation.extra_balls,
                rocket: nextProps.reservationInfo.data.result.reservation.extra_rocket,
                newPrice: Number(nextProps.reservationInfo.data.result.reservation.summ),
                phone: nextProps.reservationInfo.data.result.reservation.phone,
                username: nextProps.reservationInfo.data.result.reservation.username
         })
        }
        if (this.props.canceledReserve != nextProps.canceledReserve) {
            this.setState({
                loaded: true,
                duplicateResultMessage: nextProps.canceledReserve.message,
            })
            nextProps.canceledReserve.status == 200 ? this.props.close('free') : '';
        }
        if (this.props.confirmed != nextProps.confirmed) {
            this.setState({
                loaded: true,
                duplicateResultMessage: nextProps.confirmed.message
            })
            nextProps.confirmed.status == 200 ? this.props.close('confirmed') : '';
        }
        if (this.props.duplicated != nextProps.duplicated) {
            //console.log('Duplicated error handler',nextProps.duplicated.message)
            this.setState({
                loaded: true,
                duplicateResultMessage: nextProps.duplicated.message
            })
            nextProps.duplicated.status == 200 ? this.props.close('hiden') : '';
        }


    }


    convertToDates(dates) {
        let dts = this.state.duplicatedDate;

            if(dates.length == 0) {
                dts.push(this.state.date);
                console.log('Dates object', dts)
                return dts;
            } else {
                dates.forEach((date) => {
                    dts.push(new Date(date))
                });
                console.log('Dates object', dts)
                return dts;
            }
    }

    submit =(model) => {
        console.log('confirm', model, this.state)

        this.props.confirmReserve({
            date: this.state.date,
            court: this.state.court,
            username: model.username,
            phone: model.phone,
            hour: this.state.hour,
            price: this.state.newPrice,
            discount: this.state.discount,
            coach: this.state.coach,
            balls: this.state.balls,
            rocket: this.state.rocket,
            status: 'confirmed',
            duplicatedDate: this.state.duplicatedDate
        })
        this.setState({loaded : false})
    }

    cancel = (e) => {
            console.log('cancel')
            this.props.cancelReserve({
                date: this.state.date,
                hour: this.state.hour,
                court: this.state.court
            })
        this.setState({loaded : false})
    }

    duplicate = () => {
        this.props.duplicate({
            duplicate: this.state.duplicatedDate,
            date: this.state.date,
            hour: this.state.hour,
            court: this.state.court
        })
        this.setState({loaded : false})
    }

    changeExtrasState = (e) => {
        console.log('Change state', e.target.name)
        let extraType = e.target.name,
            extraMoney = 0;
        switch (e.target.name) {
            case 'coach' :
                console.log('Coach case', e.target.name == 'coach' )
                extraMoney = this.state.coachPrice - (this.state.coachPrice/100*this.state.discount);
                break;
            case 'rocket' :
                console.log('Rocket case')
                extraMoney = this.state.rocketPrice - (this.state.rocketPrice/100*this.state.discount);
                break;
            case 'balls' :
                console.log('Balls case')
                extraMoney = this.state.ballsPrice - (this.state.ballsPrice/100*this.state.discount);
                break;
            default :
                break;
        }
        console.log('Extra money result', extraMoney)

        if(e.target.checked) {
        this.setState({
            [e.target.name]: e.target.checked,
            newPrice: this.state.newPrice + extraMoney
        })
        } else {
            this.setState({
                [e.target.name]: e.target.checked,
                newPrice: this.state.newPrice - extraMoney >= 0 ? this.state.newPrice - extraMoney  : 0
            })
        }
    }

    handleDayClick = (day, { selected }) => {
        let { duplicatedDate, newPrice } = this.state;
        console.log('DuplicatedDate object', duplicatedDate)
        if (selected) {

            const selectedIndex = duplicatedDate.findIndex(selectedDay =>
                DateUtils.isSameDay(selectedDay, day)
            );
            duplicatedDate.splice(selectedIndex, 1);

        } else {

            let d = new Date(day.setHours(this.state.hour+3))
            console.log('Added day', duplicatedDate, this.state.date)
            duplicatedDate.push(d);
           // newPrice = newPrice + new
        }

        this.setState({ duplicatedDate });
    };


    calculateDiscount = (e) => {
        let price = this.state.courtPrice
            + (this.state.coach ? this.state.coachPrice : 0)
            + (this.state.rocket ? this.state.rocketPrice : 0)
            + (this.state.balls ? this.state.ballsPrice : 0),

            discount = parseInt(e.currentTarget.value);
        console.log('Discount', discount)
        console.log('Is NaN', isNaN(discount))
        if(discount == 0 || isNaN(parseInt(discount)) || discount > 100 ) {
            e.currentTarget.value = '';
            this.setState({
                discount: '',
                newPrice: this.state.courtPrice
                + (this.state.coach ? this.state.coachPrice : 0)
                + (this.state.rocket ? this.state.rocketPrice : 0)
                + (this.state.balls ? this.state.ballsPrice : 0)
            })
        } else {
            price = discount != 0 //|| isNaN(parseInt(e.currentTarget.value))
                ? price - (price / 100 * discount)
                : price;
            console.log('Change state', e.currentTarget.value);
            this.setState({
                newPrice: Math.round(price, 0),
                discount: e.currentTarget.value
            })
        }
    }

    render () {

        const _this = this;

        return (
            <Formsy.Form onValidSubmit={this.submit} className="ManageReservation">
                { this.state.loaded ?
                    <Table responsive>
                    <thead>
                    <tr className="header">
                    <th colSpan="4">{this.state.header}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr className="title">
                    <td colSpan="3">Подтвердите</td>
                    <td rowSpan="5" className="calendar">
                    <DayPicker
                    selectedDays={this.state.duplicatedDate}
                    onDayClick={this.handleDayClick}
                    />
                    </td>
                    </tr>
                    <tr>
                    <td colSpan="3">
                    <MyOwnInput autoFocus={false} defStyle="smart__field--tel--admin" placeholder="Телефон" type="text" name="phone" value={this.state.phone}/>
                    </td>
                    </tr>
                    <tr>
                    <td colSpan="3">
                    <MyOwnInput autoFocus={false} type="text" defStyle="smart__field--guest--admin" placeholder="Имя фамилия" name="username" value={this.state.username}/>
                    </td>
                    </tr>
                    <tr>
                    <td colSpan="2" className="extras">
                    <Checkbox placeholder="Теренер" name="coach" checked={this.state.coach} onClick={this.changeExtrasState}>
                    Тренер   1000 р/ч
                    </Checkbox>
                    <Checkbox placeholder="Ракетка" name="rocket" checked={this.state.rocket} onClick={this.changeExtrasState}>
                    Ракетка  300 р/ч
                    </Checkbox>
                    <Checkbox placeholder="Мячи" name="balls" checked={this.state.balls} onClick={this.changeExtrasState}>
                    Мячи     100 р/ч
                    </Checkbox>
                    </td>
                    <td className="discount_duplicate">
                    <label className="label">Скидка</label><br/>
                    <input type="text" value={this.state.discount == 0 ? '' : this.state.discount} placeholder="0 %" onChange={this.calculateDiscount} className="discount"/>
                    <Button className="button-red duplicate" onClick={this.duplicate} disabled={this.state.status == 'free' || this.state.status == 'holden' ? true : false}>Дублировать</Button><br/>
                    <label className="label">{this.state.duplicateResultMessage == false ? "Дублировать аренду на другие дни" : this.state.duplicateResultMessage}</label>
                    </td>
                    </tr>
                    <tr>
                    <td colSpan="3" className="duplicate_month">

                    {/*{_.map(this.state.months, function (month, i) {
                     return <DuplicateMonths handleMonths={_this.addDuplicate}
                     month={month}
                     duplicatedMonth={_this.state.duplicatedMonth}
                     key={month}/>;
                     })
                     }*/}

                    </td>
                    </tr>

                    <tr>
                    <td className="cancel">
                    <Button type="submit" className="button-red" onClick={this.cancel} disabled={this.state.status == 'free' ? true : false}>Отмена брони</Button>
                    </td>

                    <td colSpan="2"></td>
                    <td className="confirm">
                    <label>Общая сумма </label><span className="money"></span> <label>{this.state.newPrice * this.state.duplicatedDate.length} р.</label>
                    <Button type="submit" className="button-red">ОК</Button>
                    </td>
                    </tr>
                    </tbody>
                    </Table>
                    :  <Loader loaded={this.state.loaded}/>
                }
            </Formsy.Form>
        )
    }
}

function mapStateToProps({confirmReserve, cancelReserve, duplicate, getReservationInfo, getDefaultDate, getNotifications}) {
    return {
        confirmReserve,
        cancelReserve,
        duplicate,
        getReservationInfo,
        getDefaultDate,
        getNotifications
    };
}

export default connect(mapStateToProps, { confirmReserve, cancelReserve, duplicate, getReservationInfo, getDefaultDate, getNotifications })(ManageReservationForm);

