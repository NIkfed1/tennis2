import React, { Component } from 'react';
import { Col, Button, Form, FormGroup, FormControl, ControlLabel, Clearfix, Tab, Row, Nav, NavItem, Table, Column, Checkbox } from 'react-bootstrap';

export default class DuplicatedMonth extends Component {
    constructor(props) {
        super(props)
        console.log('Props', props)
        this.state = {
            duplicatedMonth: this.props.duplicatedMonth
        }
    }

    render() {
            return (
                <Button value={this.props.month}
                        className={this.props.duplicatedMonth.indexOf(this.props.month) != -1 ? 'active' : 'inactive'}
                        onClick={this.props.handleMonths}>
                    {this.props.month}</Button>)
    }
}

