import React, { Component } from 'react';
import Formsy from 'formsy-react';

import '../css/UITable.css'

const MyOwnInput = React.createClass({
    mixins: [Formsy.Mixin],
    changeValue(event) {
        this.setValue(event.currentTarget.value);
    },

    render() {
        const className = this.showRequired() ? 'required' : this.showError() ? 'error' : null;
        const errorMessage = this.getErrorMessage();
        const defStyle = this.props.defStyle;
        const type = this.props.type;
        const checked = type == 'checkbox' || this.getValue == true ? 'checked' : '';

        return (
            <div className={defStyle}>
                <input autoFocus={this.props.autoFocus}
                       type={type}
                       placeholder={this.props.placeholder}
                       onChange={this.changeValue}
                       value={this.getValue() }
                />

                {this.props.label ?
                    <label>{this.props.label}</label>
                    : ''}

                <div>{errorMessage}</div>
            </div>
        );
    }
});


export  default MyOwnInput;