import React, { Component, PropTypes } from 'react';

import { bindActionCreators } from 'redux'
import * as TodoActions from '../actions/ToDoActions';
import * as AuthAction from '../actions/AuthActions';
import * as LoginPageActions from '../actions/LoginPageActions';
import * as AddReservationAction from '../actions/AddReservationAction';
import { connect } from 'react-redux';

import { observer } from 'mobx-react';
import { IndexRoute, Router, Route, Link, browserHistory, hashHistory } from 'react-router';

import LayHeader from './LayHeader';
import LayFooter from './LayFooter';
import UITable from './UITable';
import LoginOrRegister from './LoginOrRegister';

import './css/App.css';
import './css/index.css';
import './css/RD.css';


@connect(state => ({
    todos: state.todoss,
    user: state.user,
    loginPage: state.loginPage,
    addReservation: state.addReservation
}))
export default class Layout extends Component {
    constructor(props) {
        super(props);
        console.log('Layout. Constructor', props);
    }

    componentWillUpdate(nextProps) {
        console.log('Layout. ComponentWillUpdate', nextProps);
    }


    render() {
        const { todos, user, loginPage, dispatch, addReservation } = this.props;

        return (
            <div className="App">
                <LayHeader todos={todos}
                           loginPage={loginPage}
                           user={user}
                           admin={false}
                           {...bindActionCreators(TodoActions, dispatch)}
                           {...bindActionCreators(AuthAction, dispatch)}
                           {...bindActionCreators(LoginPageActions, dispatch)}/>
                {
                    loginPage.state ?
                    <LoginOrRegister loginPage={loginPage}
                                     user={user}
                                     {...bindActionCreators(AuthAction, dispatch)}
                                     {...bindActionCreators(LoginPageActions, dispatch)}/> : ''
                }
                <div className='flexcontainer'>
                    <UITable todos={todos}
                             addReservation={addReservation}
                                 court={0}
                                 {...bindActionCreators(TodoActions, dispatch)}
                             {...bindActionCreators(AddReservationAction, dispatch)}

                    />
                    <UITable todos={todos}
                             addReservation={addReservation}
                                 court={1}
                                 {...bindActionCreators(TodoActions, dispatch)}
                                 {...bindActionCreators(AddReservationAction, dispatch)}
                    />

                </div>

                <LayFooter loginPage={loginPage}
                           user={user}
                           admin={false}
                           {...bindActionCreators(AuthAction, dispatch)}
                           {...bindActionCreators(LoginPageActions, dispatch)}/>
            </div>
        );

    }
};
